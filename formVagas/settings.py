"""
Django settings for formVagas project.

Generated by 'django-admin startproject' using Django 3.2.5.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""

import os
import environ
import time



from pathlib import Path

# set default values and casting
env = environ.Env(
    DEBUG=(bool, True),
    SECURE_SSL_REDIRECT=(bool, False)
    )
environ.Env.read_env() # reading .env file


# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-^uali8c)y#)#=#&30%(guw**)kh25udc7l86_vp(@j)8c0^1%7'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env('DEBUG')


ALLOWED_HOSTS = ['*',]

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_object_actions',
    'crispy_forms',
    "crispy_bootstrap5",
    'vagas',
    'rangefilter',
    'django_admin_listfilter_dropdown',
    'satisfacao',
    'paginaDeCaptura'

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'formVagas.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'vagas/templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'formVagas.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql',
#         'NAME': 'banco1',
#         'USER': 'postgres',
#         'PASSWORD':'123456',
#         'HOST':'localhost'
#     }
# }
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'pt-br'

TIME_ZONE = 'America/Sao_Paulo'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'formVagas/static')
]

# Media
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

CRISPY_ALLOWED_TEMPLATE_PACKS = "bootstrap5"
CRISPY_TEMPLATE_PACK = "bootstrap5"

PHONENUMBER_DEFAULT_REGION ='BR'

from django.contrib.messages import constants as messages
MESSAGE_TAGS = {
    messages.ERROR: 'danger',
    messages.SUCCESS: 'success',
}

expires = time.time() + 6 * 24 * 3600 # 6 days from now


AWS_ACCESS_KEY_ID = os.environ.get('AWSAccessKeyId', 'AKIA2LNO2WBQH5V3J5M2')
AWS_SECRET_ACCESS_KEY = os.environ.get(
    'AWSSecretKey', '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')
AWS_STORAGE_BUCKET_NAME = os.environ.get('S3_BUCKET_NAME', 'gorilamedias')
AWS_QUERYSTRING_AUTH = False
AWS_S3_REGION_NAME = os.environ.get('AWS_S3_REGION_NAME', 'sa-east-1')
AWS_IS_GZIPPED = True

AWS_S3_OBJECT_PARAMETERS = {
    'Expires': time.strftime("%a, %d-%b-%Y %T GMT", time.gmtime(expires)),
    'CacheControl': 'max-age=86400',
}

WHITENOISE_MAX_AGE = 86400

STATICFILES_LOCATION = '/static/'

STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
COMPRESS_STORAGE = DEFAULT_FILE_STORAGE

ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'



MEDIA_URL = 'https://s3-'+AWS_S3_REGION_NAME+'.amazonaws.com/' \
    + AWS_STORAGE_BUCKET_NAME+'/'


import django_heroku
django_heroku.settings(locals())

