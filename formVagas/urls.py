import vagas
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('seletivo/', include('vagas.urls')),
    path('pesquisa/', include('satisfacao.urls')),
    path('masterclass/', include('paginaDeCaptura.urls')),
    path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
