from django.contrib import admin
from rangefilter.filters import DateRangeFilter

from paginaDeCaptura.models import Cupom, GoBig

# Register your models here.

from vagas.actions import export_as_csv_gobig_c


def download_csv(modeladmin, request, queryset):
    import csv
    f = open('some.csv', 'wb')




class ListGoBig(admin.ModelAdmin):
    list_display = ('name','cpf','phone_number','email','paid','created_at', 'cupom', 'indicacao')
    list_display_links = ('name',)
    list_filter = (('created_at', DateRangeFilter), 'paid', 'indicacao')
    search_fields = ('name', 'cpf', 'phone_number', 'email')
    list_per_page = 50
  
    actions = [export_as_csv_gobig_c("Exportar csv com incritos selecionados")]

class Listcupom(admin.ModelAdmin):
    list_display = ('companny', 'code', 'active')
    list_display_links = ('companny', 'code')
    list_filter = ('active',)
    search_fields = ('companny', 'code')
    list_per_page = 50  


admin.site.register(GoBig, ListGoBig)
admin.site.register(Cupom, Listcupom)
