from django.shortcuts import redirect, render

from paginaDeCaptura.models import Cupom, GoBig
from django.contrib import messages

# Create your views here.



def gobig_success(request):
  return render(request, 'paginaDeCaptura/success.html')

def gobig(request):
    if request.method == 'POST':

        name = request.POST['name']
        email = request.POST['email']
        phone_number = request.POST['phone_number']
        cpf = request.POST['cpf']
        cupom = request.POST['cupom']
        indicacao = request.POST['indicacao']
        if cupom: 
          find = Cupom.objects.filter(code=cupom)
          if find and find.get().active:
            gfind = GoBig.objects.filter(email=email)
            gfindcpf = GoBig.objects.filter(cpf=cpf)
            if gfind:
              messages.add_message(request, messages.ERROR, 'Email já cadastrado. tente novamente!')
              return render(request, 'paginaDeCaptura/lp.html', {'name': name, 'email': email, 'cpf': cpf, 'phone_number': phone_number,'indicacao': indicacao , 'cupom':cupom })
            if  gfindcpf:
              messages.add_message(request, messages.ERROR, 'CPF já cadastrado. tente novamente!')
              return render(request, 'paginaDeCaptura/lp.html', {'name': name, 'email': email, 'cpf': cpf, 'phone_number': phone_number, indicacao: 'indicacao','cupom':cupom })
            Gobig = GoBig.objects.create(
            name=name,
            email=email,
            phone_number=phone_number,
            cpf=cpf,
            cupom=find.get(),
            indicacao= indicacao
            )
            return redirect('gobig_success')
          else:
            messages.add_message(request, messages.ERROR, 'Cupom inválido!')
            return render(request, 'paginaDeCaptura/lp.html', {'name': name, 'email': email, 'cpf': cpf, 'phone_number': phone_number, 'indicacao':indicacao })
        # Gobig.save()
        else:
          gfind = GoBig.objects.filter(email=email)
          gfindcpf = GoBig.objects.filter(cpf=cpf)
          if gfind:
            messages.add_message(request, messages.ERROR, 'Email já cadastrado. tente novamente!')
            return render(request, 'paginaDeCaptura/lp.html', {'name': name, 'email': email, 'cpf': cpf, 'phone_number': phone_number, 'indicacao':indicacao,'cupom':cupom })
          if  gfindcpf:
            messages.add_message(request, messages.ERROR, 'CPF já cadastrado. tente novamente!')
            return render(request, 'paginaDeCaptura/lp.html', {'name': name, 'email': email, 'cpf': cpf, 'phone_number': phone_number, 'indicacao':indicacao, 'cupom':cupom })
          Gobig = GoBig.objects.create(
            name=name,
            email=email,
            phone_number=phone_number,
            cpf=cpf,
            indicacao=indicacao
          )
          return redirect('gobig_success')  
    else:
        # if request.GET:
        #   cupom = request.GET['cupom']
        #   if cupom:
        #     find = Cupom.objects.filter(code=cupom)
        #     if find and find.get().active:
        #       messages.add_message(request, messages.SUCCESS, 'Cupom de desconto adicionado!', )
        #       return render(request, 'paginaDeCaptura/lp.html', {'cupom':find.get().code, 'isValidCupom':True })
        #     else:
        #       messages.add_message(request, messages.ERROR, 'Cupom inválido!')
        #       return render(request, 'paginaDeCaptura/lp.html', {'cupom':find.get().code, 'isValidCupom':False })

        # messages.success(request, 'Profile details updated.')
        return render(request, 'paginaDeCaptura/lp.html')
