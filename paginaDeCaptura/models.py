from datetime import time
from django.db import models
from django.db.models.deletion import CASCADE
from django.db.models.fields import DateTimeField
from django.utils.timezone import now
from phonenumber_field.modelfields import PhoneNumberField
from django.db.models.signals import post_save
from django.contrib.admin.models import LogEntry
from django.contrib.auth.models import User

import requests
import random



# Create your models here.

class Cupom(models.Model):
    companny= models.CharField(max_length=100, verbose_name="empresa")
    code= models.CharField(max_length=50, unique=True,verbose_name="Código do cupom")
    active= models.BooleanField(default=True, verbose_name="Ativo")
    created_at = DateTimeField(default=now, editable=False, verbose_name='Data de criação')
    def __str__(self) -> str:
        return self.code

class GoBig(models.Model):
    name = models.CharField(max_length=200, verbose_name="nome")
    cpf = models.CharField(unique=True, max_length=14, verbose_name="CPF")
    phone_number = PhoneNumberField(verbose_name='Telefone')
    email = models.CharField(unique=True,max_length=200, verbose_name="Email")
    paid = models.BooleanField(default=False, max_length=200, verbose_name="Pago")
    cupom = models.ForeignKey(Cupom, blank=True, null=True,default=None,editable=False, verbose_name='Cupom de desconto', on_delete=CASCADE)
    indicacao = models.CharField(max_length=200, blank=True, null=True, default=None, verbose_name='Indicação')
    created_at = DateTimeField(default=now, editable=False, verbose_name='Data de criação')
    def __str__(self) -> str:
        return self.name
    class Meta:
        verbose_name = 'Go Big Inscrito'
        verbose_name_plural = 'Go Big Inscritos'

def save_post(sender, instance, created, **kwargs):
  u = User.objects.all()
  tel= '%s' % u[0].last_name
  token= '%s' % u[0].first_name
  newTel = '%s' % instance.phone_number
  if created:
    if instance.cupom ==None:
      msg = '🦍 Novo cadastro:\n\n✅ Nome: %s\n☎️ Telefone:\nhttps://wa.me/%s\n📧 Email: %s\n🎖 Indicado por: %s' % (instance.name, newTel.replace("+", ""), instance.email, instance.indicacao)
    else:
      msg = '🦍 Novo cadastro:\n\n✅ Nome: %s\n☎️ Telefone:\nhttps://wa.me/%s\n📧 Email: %s\n🎫 Cupom: %s\n🎖 Indicado por: %s' % (instance.name, newTel.replace("+", ""), instance.email, instance.cupom, instance.indicacao)
    msgClient = 'Olá *%s*!\nObrigado por cadastrar no evento *GORILAS GO BIG*! 🚀🦍\n\nNós recebemos o seu pedido de Inscrição no evento MASTERCLASS GORILAS GO BIG e agora vamos gerar o boleto para pagamento. O proximo passo é a confirmação do pagamento para que você esteja 100%s confirmado! Uma mensagem será enviada assim que houver a confirmação.\n\nAh, você já convidou alguém pro evento? Quem tiver mais convidados presentes receberá um premio dos Gorilas!' % (instance.name, "%")
    msg_full = 'https://v1.utalk.chat/send/%s/?cmd=chat&id=dsahdjashj&to=%s@c.us&msg=%s' % (token, tel, msg)
    msg_full_client = 'https://v1.utalk.chat/send/%s/?cmd=chat&id=dsahdjashj&to=%s@c.us&msg=%s' % (token, newTel.replace("+", ""), msgClient)
    # print(token)
    requests.get(msg_full)
    requests.get(msg_full_client)
    return
  else:
    if instance.paid: 
      msgClient = 'Olá, *%s*! O pagamento da sua inscrição na *MASTERCLASS GORILAS GO BIG*  acabou de ser aprovado ✅.\n\nLembrando que o evento acontecerá no dia 19/02, 14:00 as 18:00, no Auditório do Aracati Office.\n\nFalando nisso, conta pra gente, qual segmento da sua empresa, ou em qual área você se interessa para aplicação do planejamento estratégico.' % instance.name
      msg_full_client = 'https://v1.utalk.chat/send/%s/?cmd=chat&id=dsahdjashj&to=%s@c.us&msg=%s' % (token, newTel.replace("+", ""), msgClient)
      requests.get(msg_full_client)
post_save.connect(save_post, sender=GoBig, dispatch_uid=random.getrandbits(128))
