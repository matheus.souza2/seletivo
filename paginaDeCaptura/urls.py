from django.urls import path

from . import views

urlpatterns = [
    path('gobig/', views.gobig, name='gobig'),
    path('gobig/sucesso/', views.gobig_success, name='gobig_success'),
]