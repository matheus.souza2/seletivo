# Generated by Django 3.2.5 on 2022-01-13 23:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paginaDeCaptura', '0009_alter_gobig_cupom'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gobig',
            name='cupom',
            field=models.CharField(blank=True, default=None, max_length=200, null=True, verbose_name='Indicação'),
        ),
    ]
