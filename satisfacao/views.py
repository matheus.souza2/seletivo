from django.shortcuts import redirect, render

from satisfacao.models import Atendentes, Elleva

# Create your views here.


def elleva(request):
    if request.method == 'POST':
        espera_atendimento = request.POST['espera_atendimento']
        atendentes_educados = request.POST['atendentes_educados']
        problema_resolvido = request.POST['problema_resolvido']
        delicadeza_cuidado = request.POST['delicadeza_cuidado']
        limpeza_conforto = request.POST['limpeza_conforto']
        informacoes_recebidas = request.POST['informacoes_recebidas']
        probabilidade_recomendacao = request.POST['probabilidade_recomendacao']
        desconforto = request.POST['desconforto']
        desconforto_texto = request.POST['desconforto_texto']
        experiencia = request.POST['experiencia']
        sugestao = request.POST['sugestao']
        atendente = request.POST.getlist('atendente')
        elleva = Elleva.objects.create(
            espera_atendimento = espera_atendimento,
            atendentes_educados = atendentes_educados,
            problema_resolvido = problema_resolvido,
            delicadeza_cuidado = delicadeza_cuidado,
            limpeza_conforto = limpeza_conforto,
            informacoes_recebidas = informacoes_recebidas,
            probabilidade_recomendacao = probabilidade_recomendacao,
            desconforto = desconforto,
            desconforto_texto = desconforto_texto,
            experiencia = experiencia,
            sugestao = sugestao,
        )
        elleva.atendente.set(Atendentes.objects.filter(pk__in=atendente))
        elleva.save()
        return redirect('pesquisa-enviada')
    else:
        atendentes= Atendentes.objects.all()
        return render(request, 'satisfacao.html', {'atendentes': atendentes})

def pesquisa_confirmada(request):
    return render(request, 'pesquisa-enviada.html')
def resultado(request):
    espera_atendimento = 0
    atendentes_educados = 0
    problema_resolvido = 0
    delicadeza_cuidado = 0
    limpeza_conforto = 0
    informacoes_recebidas = 0
    probabilidade_recomendacao = 0
    desconforto = {
      'sim': 0,
      'nao': 0
    }
    resultado=Elleva.objects.all()
    leng = len(resultado)
    for r in resultado:
        espera_atendimento = espera_atendimento + r.espera_atendimento/leng
        atendentes_educados = atendentes_educados + r.atendentes_educados/leng
        problema_resolvido = problema_resolvido + r.problema_resolvido/leng
        delicadeza_cuidado = delicadeza_cuidado + r.delicadeza_cuidado/leng
        limpeza_conforto = limpeza_conforto + r.limpeza_conforto/leng
        informacoes_recebidas = informacoes_recebidas + r.informacoes_recebidas/leng
        probabilidade_recomendacao = probabilidade_recomendacao + r.probabilidade_recomendacao/leng
        if r.desconforto == 'Sim':
          desconforto['sim'] += 1
        else:
          desconforto['nao'] +=1
    return render(request, 'resultado.html', {'resultado':resultado, 'espera_atendimento':espera_atendimento,
        'atendentes_educados':atendentes_educados,
        'problema_resolvido':problema_resolvido,
        'delicadeza_cuidado':delicadeza_cuidado,
        'limpeza_conforto':limpeza_conforto,
        'informacoes_recebidas':informacoes_recebidas,
        'probabilidade_recomendacao':probabilidade_recomendacao,
        'desconforto': desconforto
    })