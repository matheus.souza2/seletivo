from django.contrib import admin
from rangefilter.filters import DateRangeFilter, DateTimeRangeFilter
from django.contrib.admin import AdminSite

from satisfacao.models import Atendentes, Elleva

# Register your models here.

class ListElleva(admin.ModelAdmin):
    class Media:
        js = ('js/admin.js',)
    list_display = (
        'id',
        'espera_atendimento',
        'atendentes_educados',
        'problema_resolvido',
        'delicadeza_cuidado',
        'limpeza_conforto',
        'informacoes_recebidas',
        'probabilidade_recomendacao',
        'desconforto',
        'desconforto_texto',
        'experiencia',
        'sugestao',
        'created_at',)
    list_display_links = ('id',)
    filter_horizontal = ['atendente']
    list_filter = (('created_at', DateRangeFilter),)
    list_per_page = 50

admin.site.register(Elleva, ListElleva)
admin.site.register(Atendentes)


admin.site.site_header = 'Gorila insight admin'