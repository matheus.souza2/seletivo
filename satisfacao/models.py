from django.db import models

from django.utils.timezone import now
from django.db.models.fields import DateField

# Create your models here.

class Atendentes(models.Model):
    name = models.CharField(max_length=200, verbose_name="nome")
    def __str__(self) -> str:
        return self.name

class Elleva(models.Model):
    DESCONFORTO_CHOICE=(
        ('Sim', 'Sim'),
        ('Não', 'Não'),
    )
    RATING_CHOICE = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (6, 6),
        (7, 7),
        (8, 8),
        (9, 9),
        (10, 10),
    )
    espera_atendimento = models.DecimalField(choices=RATING_CHOICE, max_digits=2,decimal_places=0,verbose_name="Tempo de espera")
    atendentes_educados = models.DecimalField(choices=RATING_CHOICE, max_digits=2,decimal_places=0, verbose_name="Atendentes educadas e prestativas")
    problema_resolvido = models.DecimalField(choices=RATING_CHOICE, max_digits=2,decimal_places=0, verbose_name="Problema foi resolvido?")
    delicadeza_cuidado = models.DecimalField(choices=RATING_CHOICE, max_digits=2,decimal_places=0, verbose_name="Delicadeza e cuidado")
    limpeza_conforto = models.DecimalField(choices=RATING_CHOICE, max_digits=2,decimal_places=0, verbose_name="Limpeza e conforto")
    informacoes_recebidas = models.DecimalField(choices=RATING_CHOICE, max_digits=2,decimal_places=0, verbose_name="Informações recebidas")
    probabilidade_recomendacao = models.DecimalField(choices=RATING_CHOICE, max_digits=2,decimal_places=0, verbose_name="Probabilidade de recomendação")
    desconforto = models.CharField(choices=DESCONFORTO_CHOICE, max_length=3, verbose_name="Desconforto no atendimento")
    desconforto_texto = models.TextField( verbose_name="Desconforto no atendimento" ,blank=True, null=True, default=None)
    experiencia = models.TextField( verbose_name="experiencia vivida")
    sugestao = models.TextField( verbose_name="Sugestão",blank=True, null=True, default=None)
    atendente = models.ManyToManyField(Atendentes, verbose_name="atendentes")
    created_at = DateField(default=now, editable=False, verbose_name='Data de criação')


