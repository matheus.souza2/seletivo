from django.apps import AppConfig


class SatifacaoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'satisfacao'
