from django.urls import path

from . import views

urlpatterns = [
    path('ipp/', views.elleva, name='elleva'),
    path('ipp/pesquisa-enviada', views.pesquisa_confirmada, name='pesquisa-enviada'),
    path('ipp/resultado', views.resultado, name='resultado'),

]