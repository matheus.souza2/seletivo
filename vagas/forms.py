from django import forms
from django.forms.widgets import HiddenInput
from .models import Cnh, Computing, Curriculo, Interests_area, Vaga




class CurriculoModelForms(forms.ModelForm):
    def __init__(self, vaga_pk=None,*args, **kwargs):
        vaga = Vaga.objects.get(pk=vaga_pk)
        super(CurriculoModelForms, self).__init__(*args, **kwargs)
        cnh_choices = [(choice.pk, choice) for choice in Cnh.objects.all()]
        interests_choices = [(choice.pk, choice) for choice in Interests_area.objects.all()]
        if(interests_choices):
            self.fields['interests_area'] = forms.MultipleChoiceField(required=False,widget=forms.widgets.CheckboxSelectMultiple, label='Áreas de interesses',choices=interests_choices)
        else:
            self.fields['interests_area'].widget = forms.HiddenInput() 

        computing_choices = [(choice.pk, choice) for choice in Computing.objects.all()]
        self.fields['cnh'] = forms.MultipleChoiceField(required=False ,widget=forms.widgets.CheckboxSelectMultiple, choices=cnh_choices)
        self.fields['computing'] = forms.MultipleChoiceField(required=False, widget=forms.widgets.CheckboxSelectMultiple, label='Informática',choices=computing_choices)
        self.fields['job_vacancies'].widget = forms.HiddenInput()
        self.fields['job_vacancies'].initial = vaga

    class Meta:
        model = Curriculo
        fields = [
        'job_vacancies',
        'name',
        'street',
        'district',
        'city',
        'state',
        'phone_number',
        'email',
        'postal_code',
        'gender',
        'marital_status',
        'sons',
        'birth_date',
        'pne',
        'type_of_npecial_needs',
        'cnh',
        'own_driving',
        'interests_area',
        'schooling',
        'institutions_and_courses',
        'english',
        'spanish',
        'french',
        'other_language',
        'computing',
        'others_courses',
        'companny1',
        'start_date_companny1',
        'end_date_companny1',
        'office_companny1',
        'activitie_companny1',
        'companny2',
        'start_date_companny2',
        'end_date_companny2',
        'office_companny2',
        'activitie_companny2',
        'companny3',
        'start_date_companny3',
        'end_date_companny3',
        'office_companny3',
        'activitie_companny3',
        'mini_curriculum',
        'photo',
        'curriculum'
    ]
