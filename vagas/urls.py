from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('success', views.success, name='success'),
    path('politica-de-privacidade/', views.termos, name='termos'),
    path('<uuid:vaga_pk>/', views.CurriculoCreate.as_view(), name='cadastrar'),
    path('vaga/<slug:slug>/', views.redirect, name='redirect')

]