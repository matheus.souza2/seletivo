import unicodecsv

from collections import namedtuple

from django.utils.translation import gettext as _
from django.http import HttpResponse
import datetime


def atribute_name(string):
    string = string.replace('-', '')
    string = string.replace('  ', ' ')
    string = string.replace(' ', '_')
    string = string.lower()
    return string


def export_as_csv_gcontact(description="Export selected objects as CSV GContacts",
                         fields=None, exclude=None, header=True):
    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """

    def export_as_csv(modeladmin, request, queryset):
        Contact = namedtuple('Contact', ['name', 'postal_code','street', 'district', 'city', 'state', 'phone_number', 'email','gender','marital_status','sons','birth_date','pne','type_of_npecial_needs','cnh','own_driving','interests_area','schooling','institutions_and_courses','english','spanish','french','other_language','computing','others_courses','companny1','start_date_companny1','end_date_companny1','office_companny1','activitie_companny1','companny2','start_date_companny2','end_date_companny2','office_companny2','activitie_companny2','companny3','start_date_companny3','end_date_companny3','office_companny3','activitie_companny3','mini_curriculum','photo','curriculum'])
        opts = modeladmin.model._meta
        if not fields:
            field_names = [field.name for field in opts.fields]
        else:
            field_names = fields

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' % str(opts).replace('.', '_')
        Contact.__new__.__defaults__ = ('',) * len(Contact._fields)
        writer = unicodecsv.writer(response, encoding='utf-8')
        header = ['name', 'postal_code','street', 'district', 'city', 'state', 'phone_number', 'email','gender','marital_status','sons','birth_date','pne','type_of_npecial_needs','cnh','own_driving','interests_area','schooling','institutions_and_courses','english','spanish','french','other_language','computing','others_courses','companny1','start_date_companny1','end_date_companny1','office_companny1','activitie_companny1','companny2','start_date_companny2','end_date_companny2','office_companny2','activitie_companny2','companny3','start_date_companny3','end_date_companny3','office_companny3','activitie_companny3','mini_curriculum','photo','curriculum']
        if header:
            writer.writerow(header)
        for obj in queryset:
            contact = Contact(  
                                name=obj.name,
                                postal_code=obj.postal_code,
                                street=obj.street,
                                district=obj.district,
                                city=obj.city,
                                state=obj.state,
                                phone_number=obj.phone_number,
                                email=obj.email,
                                gender=obj.gender,
                                marital_status=obj.marital_status,
                                sons=obj.sons,
                                birth_date=obj.birth_date,
                                pne=obj.pne,
                                type_of_npecial_needs=obj.type_of_npecial_needs,
                                cnh=obj.cnh,
                                own_driving=obj.own_driving,
                                interests_area=obj.interests_area,
                                schooling=obj.schooling,
                                institutions_and_courses=obj.institutions_and_courses,
                                english=obj.english,
                                spanish=obj.spanish,
                                french=obj.french,
                                other_language=obj.other_language,
                                computing=obj.computing,
                                others_courses=obj.others_courses,
                                companny1=obj.companny1,
                                start_date_companny1=obj.start_date_companny1,
                                end_date_companny1=obj.end_date_companny1,
                                office_companny1=obj.office_companny1,
                                activitie_companny1=obj.activitie_companny1,
                                companny2=obj.companny2,
                                start_date_companny2=obj.start_date_companny2,
                                end_date_companny2=obj.end_date_companny2,
                                office_companny2=obj.office_companny2,
                                activitie_companny2=obj.activitie_companny2,
                                companny3=obj.companny3,
                                start_date_companny3=obj.start_date_companny3,
                                end_date_companny3=obj.end_date_companny3,
                                office_companny3=obj.office_companny3,
                                activitie_companny3=obj.activitie_companny3,
                                mini_curriculum=obj.mini_curriculum,
                                photo=obj.photo,
                                curriculum=obj.curriculum,                             )
            writer.writerow([ getattr(contact,atribute_name(i))  for i in header ])

        return response
    export_as_csv.short_description = description
    return export_as_csv


def export_as_csv_gobig_c(description="Export selected objects as CSV GContacts",
                         fields=None, exclude=None, header=True):
    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """

    def export_as_csv_gobig(modeladmin, request, queryset):
        Contact = namedtuple('Contact', ['name', 'phone_number', 'email', 'cpf', 'paid', 'created_at'])
        opts = modeladmin.model._meta
        if not fields:
            field_names = [field.name for field in opts.fields]
        else:
            field_names = fields

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=inscritos_masterclass_gobig_%s.csv' % str(datetime.datetime.now().strftime ("%d-%m-%Y"))
        Contact.__new__.__defaults__ = ('',) * len(Contact._fields)
        writer = unicodecsv.writer(response, encoding='utf-8')
        header = ['Nome', 'Telefone', 'Email', 'CPF', 'Pago', 'Criado em']
        headerEn = ['name', 'phone_number', 'email', 'cpf', 'paid', 'created_at']
        if header:
            writer.writerow(header)
        for obj in queryset:
            contact = Contact(
              name=obj.name,
              phone_number=obj.phone_number,
              email=obj.email,
              cpf=obj.cpf,
              paid=obj.paid,
              created_at=obj.created_at
            )
            writer.writerow([ getattr(contact,atribute_name(i))  for i in headerEn ])

        return response
    export_as_csv_gobig.short_description = description
    return export_as_csv_gobig
