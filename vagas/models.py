from django.db import models
from django.db import models
from datetime import date
from django.db.models.base import Model
from django.db.models.deletion import CASCADE
from django.db.models.fields.related import ManyToManyField
from model_utils.fields import UUIDField
from django.utils.timezone import now
from phonenumber_field.modelfields import PhoneNumberField
from localflavor.br.br_states import STATE_CHOICES





from django.db.models.fields import DateField


class Vaga(models.Model):
    uuid = UUIDField(primary_key=True, version=4, editable=False)
    title = models.CharField(max_length=200)
    description = models.TextField()
    valid_date = models.DateField(default=None, blank=True, null=True)
    created_at = DateField(default=now, editable=False, verbose_name='Data de criação')
    image = models.ImageField(upload_to='fotos/%d/%m/%y', blank=True)
    ativa = models.BooleanField(default=False)
    slug = models.SlugField(unique=True,
        db_index=True,
        default=None,
    )

    @property
    def is_valid_date(self):
        return self.valid_date >= date.today()
    
    def __str__(self) -> str:
        return self.title


class Cnh(models.Model):
    nome = models.CharField(max_length=2)

    def __str__(self) -> str:
        return self.nome


class Interests_area(models.Model):
    area = models.CharField(max_length=100)

    def __str__(self) -> str:
        return self.area


class Computing(models.Model):
    area = models.CharField(max_length=200)

    def __str__(self) -> str:
        return self.area


class Curriculo(models.Model):
    DRIVING_CHOICE = (
       ('Não possui', 'Não possui'), ('moto', 'moto'), ('carro', 'carro'), ('caminhão', 'caminhão'), ('outros(as)', 'outros(as)'))
    ADADEMIC_CHOICES = (
        ('Nível Fundamental - Incompleto', 'Nível Fundamental - Incompleto'),
        ('Nível Fundamental - Completo', 'Nível Fundamental - Completo'),
        ('Nível Médio - Incompleto', 'Nível Médio - Incompleto'),
        ('Nível Médio - Completo', 'Nível Médio - Completo'),
        ('Graduação - Incompleto', 'Graduação - Incompleto'),
        ('Graduação - Completo', 'Graduação - Completo'),
        ('Cursos tecnológicos Sup. - Completo', 'Cursos tecnológicos Sup. - Completo'),
        ('Cursos tecnológicos Sup. - Incompleto', 'Cursos tecnológicos Sup. - Incompleto'),
        ('Curso de Extensão - Incompleto',' Curso de Extensão - Incompleto '),
        ('Curso de Extensão - Completo',' Curso de Extensão - Completo '),
        ('Especialização - Incompleto',' Especialização - Incompleto '),
        ('Especialização - Completo',' Especialização - Completo '),
        ('MBA - Incompleto',' MBA - Incompleto '),
        ('MBA - Completo',' MBA - Completo '),
        ('Mestrado - Incompleto',' Mestrado - Incompleto '),
        ('Mestrado - Completo',' Mestrado - Completo '),
        ('Doutorado - Incompleto',' Doutorado - Incompleto '),
        ('Doutorado - Completo',' Doutorado - Completo')
    )

    MATRIAL_STATUS = (
        ('solteiro', 'Solteiro(a)'),
        ('casado', 'Casado(a)'),
        ('divorciado', 'Divorciado(a)'),
        ('viuvo', 'Viúvo(a)'),
        ('outros', 'outros')
    )

    LANGUAGE_CHOICE = (
        ('não domina', 'Não domino'),
        ('básico', 'Básico'),
        ('intermediárioa', 'Intermediário'),
        ('avançado', 'Avançado'),
        ('fluente', 'Fluente'),
        ('nativo', 'Nativo')
    )
    RATING_CHOICE = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
    )
    rating = models.CharField( max_length=1,choices=RATING_CHOICE, null=True,verbose_name='Avaliação', default=None, blank=True)
    is_favorite = models.BooleanField(default=False, verbose_name='Favorito')
    job_vacancies = models.ForeignKey(Vaga, verbose_name='Vaga', on_delete=CASCADE)
    name = models.CharField(verbose_name='nome', max_length=200)
    street = models.CharField(max_length=150, blank=False, verbose_name='Rua')
    district = models.CharField(max_length=150, blank=False, verbose_name='Bairro')
    city = models.CharField(max_length=150, blank=False, verbose_name='Cidade')
    state = models.CharField(max_length=2, choices=STATE_CHOICES, blank=False, default='MA', verbose_name='Estado')
    phone_number = PhoneNumberField(verbose_name='Telefone')
    email = models.EmailField(null=False, blank=False, max_length=110)
    postal_code = models.CharField(max_length=9, blank=True, verbose_name='cep')
    gender = models.CharField(max_length=6, choices=( ('F', 'Feminino'), ('M', 'Masculino'), ('outros', 'outros')), blank=False, verbose_name='Sexo')
    marital_status = models.CharField(max_length=20, choices=MATRIAL_STATUS, blank=False, verbose_name='Estado civil')
    sons = models.CharField(max_length=20, choices=(('nenhum', 'nenhum'),('1', '1'), ('2', '2'), ('3', '3'), ('mais de 3', 'mais de 3')), blank=True, default=None, null=True, verbose_name='filhos')
    birth_date = models.DateField(blank=False, verbose_name='Data de nascimento')
    pne = models.BooleanField(default=False, verbose_name='PCD')
    type_of_npecial_needs = models.TextField(blank=True, verbose_name='Tipo de necessidade especial')
    cnh = models.ManyToManyField(Cnh, blank=True, default=None)
    own_driving = models.CharField(verbose_name='possui veículo', max_length=10, choices=DRIVING_CHOICE, blank=True, default=None, null=True)
    interests_area = models.ManyToManyField(Interests_area, verbose_name='Área de interesses',  default=None, blank=True)
    schooling = models.CharField(max_length=100, choices=ADADEMIC_CHOICES, verbose_name='Escolaridade')
    institutions_and_courses = models.TextField(blank=True, verbose_name='Instituições e cursos')
    english = models.CharField(max_length=15, choices=LANGUAGE_CHOICE, verbose_name='Inglês')
    spanish = models.CharField(max_length=15, choices=LANGUAGE_CHOICE, verbose_name='Espanhol')
    french = models.CharField(max_length=15, choices=LANGUAGE_CHOICE, verbose_name='Francês')
    other_language = models.CharField(max_length=200, verbose_name='outro idioma',blank=True)
    computing = models.ManyToManyField(Computing, verbose_name="Informática", blank=True,  default=None)
    others_courses = models.TextField(blank=True, verbose_name='Outros cursos')
    # professional datas
    companny1 = models.CharField(max_length=200, verbose_name='Empresa que trabalhou 1', blank=True)
    start_date_companny1 = models.DateField(default=None, blank=True, null=True, verbose_name='Data de início')
    end_date_companny1 = models.DateField(default=None, blank=True, null=True, verbose_name='Data de saída')
    office_companny1 = models.CharField(max_length=100, verbose_name='Cargo / Função', blank=True)
    activitie_companny1 = models.TextField(verbose_name='Descreva as principais atividades da empresa 1', blank=True)
    companny2 = models.CharField(max_length=200, verbose_name='Empresa que trabalhou 2', blank=True)
    start_date_companny2 = models.DateField(default=None, blank=True, null=True, verbose_name='Data de início')
    end_date_companny2 = models.DateField(default=None, blank=True, null=True, verbose_name='Data de saída')
    office_companny2 = models.CharField(max_length=100, verbose_name='Cargo / Função', blank=True)
    activitie_companny2 = models.TextField(verbose_name='Descreva as principais atividades da empresa 2', blank=True)
    companny3 = models.CharField(max_length=200, verbose_name='Empresa que trabalhou 3', blank=True)
    start_date_companny3 = models.DateField(default=None, blank=True, null=True, verbose_name='Data de início')
    end_date_companny3 = models.DateField(default=None, blank=True, null=True, verbose_name='Data de saída')
    office_companny3 = models.CharField(max_length=100, verbose_name='Cargo / Função', blank=True)
    activitie_companny3 = models.TextField(verbose_name='Descreva as principais atividades da empresa 3', blank=True)
    mini_curriculum = models.TextField(verbose_name='MINICURRÍCULO', blank=True)
    photo = models.ImageField(upload_to='user_fotos/%d/%m/%y', blank=True, verbose_name='enviar uma foto')
    curriculum = models.FileField(upload_to='user_curriculum/%d/%m/%y', blank=True, verbose_name='Anexar o seu currículo:')
    created_at = DateField(default=now, editable=False, verbose_name='Data de criação')
    

    def __str__(self) -> str:
        return self.name
