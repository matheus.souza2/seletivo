from django.contrib import admin
from rangefilter.filters import DateRangeFilter, DateTimeRangeFilter
from django_admin_listfilter_dropdown.filters import DropdownFilter, RelatedDropdownFilter
from .models import Cnh, Computing, Curriculo, Interests_area, Vaga
from django.db import models
from django.forms import CheckboxSelectMultiple
from .actions import export_as_csv_gcontact


def download_csv(modeladmin, request, queryset):
    import csv
    f = open('some.csv', 'wb')


class ListVagas(admin.ModelAdmin):
    list_display = ('title', 'uuid', 'valid_date', 'ativa')
    list_display_links = ('title',)
    list_filter = (('created_at', DateRangeFilter), 'ativa', 'valid_date')
    search_fields = ('title',)
    list_editable = ('ativa',)
    list_per_page = 50


class ListCurriculo(admin.ModelAdmin):
    list_display = (
        'name',
        'is_favorite',
        'rating',
        'phone_number',
        'email',
        'schooling',
        'english',
        'spanish',
        'french',
        'marital_status',
        'birth_date',
        'created_at'
    )
    list_filter = (
        ('created_at', DateRangeFilter),
        'is_favorite',
        ('rating', DropdownFilter),
        'job_vacancies',
        'gender',
        'marital_status',
        'cnh',
        'interests_area',
        ('schooling', DropdownFilter),
        'other_language',
        'english',
        'spanish',
        'french',
    )
    search_fields = (
        'name', 'postal_code', 'street', 'district', 'city', 'companny1', 'companny2', 'companny3',
    )
    filter_horizontal = ('cnh', 'interests_area')

    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }

    fieldsets = (
        ('admin', {
            'classes': ('',),
            'fields': ('job_vacancies', 'is_favorite', 'rating'),
        }),
        ('Dados pessoais', {
            'classes': ('',),
            'fields': ('name', 'postal_code', 'street', 'district', 'city', 'state', 'phone_number', 'email', 'gender',
                       'marital_status',
                       'sons',
                       'birth_date',
                       'pne',
                       'type_of_npecial_needs',
                       ),
        }),
        ('Outros dados', {
            'classes': ('collapse',),
            'fields': (
                'cnh',
                'own_driving',
                'interests_area',
                'schooling',
                'institutions_and_courses',
                'english',
                'spanish',
                'french',
                'other_language',
                'computing',
                'others_courses'
            ),
        }),
        ('Dados Profissionais', {
            'classes': ('collapse',),
            'fields': (
                'companny1',
                'start_date_companny1',
                'end_date_companny1',
                'office_companny1',
                'activitie_companny1',
                'companny2',
                'start_date_companny2',
                'end_date_companny2',
                'office_companny2',
                'activitie_companny2',
                'companny3',
                'start_date_companny3',
                'end_date_companny3',
                'office_companny3',
                'activitie_companny3',
                'mini_curriculum',
                'photo',
                'curriculum'
            ),
        }),

    )
    actions = [export_as_csv_gcontact("GContact export selected person")
               ]


admin.site.register(Vaga, ListVagas)


admin.site.register(Curriculo, ListCurriculo)

admin.site.register(Cnh)
admin.site.register(Interests_area)
admin.site.register(Computing)
