from vagas.forms import CurriculoModelForms
from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from vagas.models import Curriculo, Vaga
from django.views.generic import DetailView
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy
from django.forms import CheckboxSelectMultiple
from django.db import models


def index(request):
    return render(request, 'index.html')

def success(request):
    return render(request, 'success.html')
    
def termos(request):
    return render(request, 'termos.html')

def redirect(request, slug):
    vaga = Vaga.objects.get(slug=slug)
    link = '/seletivo/%s/' % vaga.uuid
    return HttpResponseRedirect(link) 

def vaga(request, vaga_id):
    vaga = get_object_or_404(Vaga, pk=vaga_id)

    vaga_to_show = {
        'vaga' : vaga
    }

    return render(request, 'vaga.html', vaga_to_show)
        

class VagaDetailView(DetailView):
    model = Vaga

class CurriculoCreate(CreateView):
    template_name = 'cadastro/form.html'
    model = Curriculo
    form_class = CurriculoModelForms
    success_url = reverse_lazy('success')

    def get_context_data(self, **kwargs):
        context = super(CurriculoCreate, self).get_context_data(**kwargs)
        context['vaga'] = get_object_or_404(
            Vaga, pk=self.kwargs.get('vaga_pk', ''))

        return context

    

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        kwargs = self.get_form_kwargs()
        kwargs.update({'vaga_pk':self.kwargs.get('vaga_pk', '')})
        form = form_class(**kwargs)
        return form
